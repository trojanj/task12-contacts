import React from 'react';
import { Alert, Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import * as Contacts from 'expo-contacts';
import call from 'react-native-phone-call'

export default ({route}) => {
  const {firstName, lastName, phoneNumbers, emails, id} = route.params.contact;
  const avatar = require('../../assets/avatar.png');

  const deleteContact = async id => {
    const {status} = await Contacts.getPermissionsAsync();
    if (status === 'granted') {
      await Contacts.removeContactAsync(id);
    }
  }

  const onCallHandler = () => {
    const args = {number: phoneNumbers[0].number, prompt: false}
    call(args).catch(console.error)
  }

  const onDeleteHandler = id => {
    Alert.alert(
      'Delete this contact?',
      '',
      [
        {
          text: 'Cancel',
          style: 'cancel'
        },
        {
          text: 'OK',
          onPress: () => {
            console.log('delete contact');
            // deleteContact(id)
          }
        }
      ]
    )
  }

  return (
    <View style={styles.contactWrp}>
      <Image source={avatar} style={styles.avatar}/>
      <Text style={styles.label}>Name</Text>
      <Text style={styles.contactData}>{firstName} {lastName}</Text>
      <Text style={styles.label}>Phone number</Text>
      <Text style={styles.contactData}>{phoneNumbers[0].number}</Text>
      <Text style={styles.label}>Email</Text>
      <Text style={styles.contactData}>{emails[0].email}</Text>
      <View style={styles.buttons}>
        <TouchableOpacity
          style={[styles.callBtn, styles.btn]}
          onPress={() => onCallHandler()}>
          <Text style={styles.btnText}>
            Call
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.btn} onPress={() =>
          onDeleteHandler(id)}>
          <Text style={styles.btnText}>
            Delete
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  contactWrp: {
    alignItems: 'center',
    backgroundColor: '#fff',
    minHeight: '100%',
    paddingVertical: 10,
    paddingHorizontal: 20
  },
  avatar: {
    width: 100,
    height: 100,
    marginBottom: 15
  },
  label: {
    width: '100%',
    fontSize: 13,
    marginBottom: 5
  },
  contactData: {
    width: '100%',
    fontSize: 17,
    marginBottom: 30,
    borderStyle: 'solid',
    borderBottomColor: '#ccc',
    borderBottomWidth: 1,
    paddingVertical: 5
  },
  buttons: {
    flexDirection: 'row',
    paddingHorizontal: 40,
    justifyContent: 'space-between',
    width: '100%'
  },
  callBtn: {
    marginRight: 70
  },
  btn: {
    backgroundColor: '#DFDFDF',
    paddingHorizontal: 30,
    paddingVertical: 10,
    borderRadius: 20
  },
  btnText: {
    fontSize: 18
  }

})
