import React, { useState } from 'react';
import { StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native';
import * as Contacts from 'expo-contacts';

export const CreateContact = ({navigation}) => {
  const [contactData, setContactData] = useState({
    firstName: '',
    lastName: '',
    number: '',
    email: ''
  })

  const validateData = () => {
    for (let value of Object.values(contactData)) {
      if (!value.trim()) {
        return false
      }
      return true
    }
  }

  const onSave = async () => {
    if (validateData()) {
      const contact = {
        [Contacts.Fields.FirstName]: contactData.firstName,
        [Contacts.Fields.LastName]: contactData.lastName,
        [Contacts.Fields.Emails]: [{email: contactData}],
        [Contacts.Fields.PhoneNumbers]: [{number: contactData.number}]
      };
      console.log('create contact')
      // const contactId = await Contacts.addContactAsync(contact);

      setContactData({
        firstName: '',
        lastName: '',
        number: '',
        email: ''
      })

      navigation.navigate('Home');
    }
  }

  const onChangeHandler = (key, text) => {
    setContactData(prevState => ({
      ...prevState,
      [key]: text
    }))
  }

  return (
    <View style={styles.centeredView}>
      <TextInput
        style={styles.input}
        placeholder='First Name'
        value={contactData.firstName}
        onChangeText={text => onChangeHandler('firstName', text)}/>
      <TextInput
        style={styles.input}
        placeholder='Last Name'
        value={contactData.lastName}
        onChangeText={text => onChangeHandler('lastName', text)}/>
      <TextInput
        style={styles.input}
        placeholder='Phone number'
        value={contactData.number}
        onChangeText={text => onChangeHandler('number', text)}/>
      <TextInput
        style={styles.input}
        placeholder='Email'
        value={contactData.email}
        onChangeText={text => onChangeHandler('email', text)}/>
      <TouchableOpacity
        title='Save'
        onPress={onSave}
        style={styles.btn}
        activeOpacity={0.7}>
        <Text style={styles.btnText}>Save</Text>
      </TouchableOpacity>
    </View>
  )
}

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    paddingVertical: 10,
    paddingHorizontal: 30,
    backgroundColor: '#fff'
  },
  input: {
    marginBottom: 15,
    paddingVertical: 5,
    borderStyle: 'solid',
    borderBottomWidth: 2,
    borderBottomColor: '#cccccc',
    fontSize: 16
  },
  btnText: {
    fontSize: 18,
    color: '#fff',
    textTransform: 'uppercase'
  },
  btn: {
    backgroundColor: 'cornflowerblue',
    alignItems: 'center',
    padding: 10,
    marginTop: 10,

  }
})
