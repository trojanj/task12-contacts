import React, { useState } from 'react';
import { ContactList } from '../components/ContactList';
import { SearchInput } from '../components/SearchInput';
import { CreateContactBtn } from '../components/CreateContactBtn';
import { StyleSheet, View } from 'react-native';

const Main = props => {
  const contacts = props.route.params.contacts;
  const [search, setSearch] = useState('');

  const getFilteredContacts = () => (
    contacts.filter(contact => {
      const fullName = contact.firstName + ' ' + contact.lastName;
      return fullName.toLowerCase().includes(search);
    })
  )

  return (
    <View style={styles.Main}>
      <SearchInput search={search} setSearch={setSearch}/>
      <CreateContactBtn/>
      <ContactList contacts={getFilteredContacts()}/>
    </View>
  )
}

const styles = StyleSheet.create({
  Main: {
    paddingTop: 60,
    padding: 10,
    minHeight: '100%',
    backgroundColor: '#fff'
  }
})

export default Main;
