import React from 'react';
import { View } from 'react-native';
import { ContactItem } from './ContactItem';

export const ContactList = ({contacts, loadContacts}) => {
  return (
    <View>
      {contacts.map(contact => <ContactItem
        key={contact.id}
        contact={contact}
        loadContacts={loadContacts}/>)}
    </View>
  )
}
