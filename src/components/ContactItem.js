import React from 'react';
import { Alert, Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { AntDesign } from '@expo/vector-icons';
import * as Contacts from 'expo-contacts';
import { useNavigation } from '@react-navigation/native';

export const ContactItem = ({contact}) => {
  const navigation = useNavigation();
  const {firstName, lastName, emails, id} = contact;
  const avatar = require('../../assets/avatar.png');

  const deleteContact = async id => {
    const {status} = await Contacts.getPermissionsAsync();
    if (status === 'granted') {
      await Contacts.removeContactAsync(id);
    }
  }

  const onDeleteHandler = id => {
    Alert.alert(
      'Delete this contact?',
      '',
      [
        {
          text: 'Cancel',
          style: 'cancel'
        },
        {
          text: 'OK',
          onPress: () => {
            console.log('delete contact')
            // deleteContact(id)
          }
        }
      ]
    )
  }

  const onContactHandler = () => {
    navigation.navigate('Contact', {contact});
  }

  return (
    <TouchableOpacity style={styles.contactItem} onPress={onContactHandler}>
      <View style={styles.row}>
        <Image source={avatar} style={styles.img}/>
        <View>
          <Text style={styles.name}>{firstName} {lastName}</Text>
          <Text style={styles.email}>{emails[0].email}</Text>
        </View>
      </View>
      <TouchableOpacity onPress={onDeleteHandler}>
        <AntDesign name="deleteuser" size={24} color="black"/>
      </TouchableOpacity>
    </TouchableOpacity>
  )
}

const styles = StyleSheet.create({
  contactItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: 10,
    paddingHorizontal: 15,
    backgroundColor: '#E7E9F4',
    marginBottom: 15
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  img: {
    width: 30,
    height: 30,
    marginRight: 20,
    borderRadius: 50
  },
  name: {
    fontSize: 16,
    marginBottom: 5
  },
  email: {
    fontSize: 16,
    color: '#505050'
  }
});
