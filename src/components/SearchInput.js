import React from 'react';
import { StyleSheet, TextInput } from 'react-native';

export const SearchInput = ({search, setSearch}) => {
  return (
    <TextInput
      placeholder='Search'
      style={styles.search}
      value={search}
      onChangeText={text => setSearch(text)}
    />
  )
}

const styles = StyleSheet.create({
  search: {
    marginBottom: 10,
    paddingVertical: 5,
    paddingHorizontal: 15,
    borderWidth: 2,
    borderColor: '#ccc',
    borderStyle: 'solid',
    borderRadius: 50,
    backgroundColor: '#fff',
    fontSize: 16
  }
})
