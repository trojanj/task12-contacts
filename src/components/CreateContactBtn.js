import React from 'react';
import { StyleSheet, Text, TouchableHighlight, View } from 'react-native';
import { AntDesign } from '@expo/vector-icons';
import { useNavigation } from '@react-navigation/native';

export const CreateContactBtn = () => {
  const navigation = useNavigation();

  return (
    <TouchableHighlight
      activeOpacity={0.6}
      underlayColor='#DDDDDD'
      style={{marginBottom: 10}}
      onPress={() => navigation.navigate('CreateContact')}>
      <View style={styles.btn}>
        <AntDesign name="adduser" size={20} color="black" style={styles.icon}/>
        <Text style={styles.text}>Create contact</Text>
      </View>
    </TouchableHighlight>
  )
}

const styles = StyleSheet.create({
  btn: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 15
  },
  icon: {
    marginRight: 10
  },
  text: {
    fontSize: 16
  }
})
