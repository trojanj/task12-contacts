import React, { useEffect, useState } from 'react';
import Main from './src/screens/Main';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Contact from './src/screens/Contact';
import { CreateContact } from './src/screens/CreateContact';
import * as Contacts from 'expo-contacts';

const Stack = createStackNavigator();

export default function App() {
  const [contacts, setContacts] = useState([]);

  useEffect(() => {
    loadContacts()
  }, [])

  async function loadContacts() {
    const {status} = await Contacts.requestPermissionsAsync();
    if (status === 'granted') {
      const {data} = await Contacts.getContactsAsync();
      setContacts(data);
    }
  }

  if (!contacts.length) return null;

  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Home">
        <Stack.Screen
          name="Home"
          component={Main}
          options={{headerShown: false}}
          initialParams={{contacts}}
        />
        <Stack.Screen
          name="Contact"
          component={Contact}
          options={{title: null}}
        />
        <Stack.Screen
          name="CreateContact"
          component={CreateContact}
          options={{title: null}}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
